package com.mywork.slackchat.repository;

import com.mywork.slackchat.common.Constants;
import com.mywork.slackchat.common.Error;
import com.mywork.slackchat.common.Response;
import com.mywork.slackchat.model.User;
import com.mywork.slackchat.services.RequestCallback;
import com.mywork.slackchat.services.UserService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

/**
 * Created by ali_d on 3/21/18.
 */

public class UserRepositoryTest {
    private static final String USER_ID = "ali@gmail.com";
    private static final String NICK_NAME = "ali";

    @Mock
    private UserService userService;
    @Mock
    private RequestCallback requestCallback;
    @Mock
    private Response response;

    private UserRepository userRepository;
    private ArgumentCaptor<RequestCallback> callbackCaptor;

    @Captor
    private ArgumentCaptor<RequestCallback> callbackArgumentCaptor;
    @Captor
    private ArgumentCaptor<String> nameArgumentCaptor;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        userRepository = new UserRepository(userService);
        callbackCaptor = ArgumentCaptor.forClass(RequestCallback.class);
    }

    @Test
    public void testCreateUser() {
        // Let's call the method under test
        userRepository.signUpUser(USER_ID, NICK_NAME);

        // verify user service with in userRepository get same parameter that is passed to userRepository signUpUser
        verify(userService).signUpUser(any(RequestCallback.class), eq(USER_ID), eq(NICK_NAME));
    }

    @Test
    public void testCreateUserWithCallback() {
        // Let's call the method under test
        userRepository.signUpUser(requestCallback, USER_ID, NICK_NAME);

        // verify user service with in userRepository get same parameter that is passed to userRepository signUpUser
        verify(userService).signUpUser(any(RequestCallback.class), eq(USER_ID), eq(NICK_NAME));
    }

    @Test
    public void testCreateUserWithCallbackSuccess() {
        Response response = Response.toSuccess(Constants.REQUEST_SIGNUP_USER_TAG, User.toUser(USER_ID, NICK_NAME));
        // Let's call the method under test
        userRepository.signUpUser(requestCallback, USER_ID, NICK_NAME);

        // verify user service with in userRepository get same parameter that is passed to userRepository signUpUser
        verify(userService).signUpUser(callbackCaptor.capture(), eq(USER_ID), eq(NICK_NAME));
        // call onResult on listener and pass testing data
        callbackCaptor.getValue().onResult(response);

        verify(requestCallback).onResult(response);
    }

    @Test
    public void testCreateUserWithCallbackFail() {
        Response response = Response.toError(Constants.REQUEST_SIGNUP_USER_TAG, Error.ERROR_MISSING_PHONE);
        // Let's call the method under test
        userRepository.signUpUser(requestCallback, USER_ID, NICK_NAME);

        // verify user service with in userRepository get same parameter that is passed to userRepository signUpUser
        verify(userService).signUpUser(callbackCaptor.capture(), eq(USER_ID), eq(NICK_NAME));
        // call onError on listener and pass testing data
        callbackCaptor.getValue().onResult(response);

        verify(requestCallback).onResult(response);
    }

    @Test
    public void testFetchAllUsersSuccess() {
        userRepository.fetchAllUsers(requestCallback);

        verify(userService).fetchAllUsers(any(RequestCallback.class));
    }
}
