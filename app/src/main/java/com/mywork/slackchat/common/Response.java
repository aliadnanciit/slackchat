package com.mywork.slackchat.common;

/**
 * Created by ali_d on 3/21/18.
 */

public class Response {
    private String tag;
    private Object data;
    private Error error;

    private Response(String tag, Object data, Error error) {
        this.tag = tag;
        this.data = data;
        this.error = error;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public static Response toResponse(String tag, Object data, Error error) {
        return new Response(tag, data, error);
    }
    public static Response toSuccess(String tag, Object data) {
        return new Response(tag, data, Error.ERROR_NONE);
    }
    public static Response toError(String tag, Error error) {
        return new Response(tag, null, error);
    }

}
