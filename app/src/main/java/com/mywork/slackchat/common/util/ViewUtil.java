package com.mywork.slackchat.common.util;

import android.view.View;

public class ViewUtil {

    public static void setEnabled(View view, boolean enabled) {
        if(view == null) {
            return;
        }
        view.setEnabled(enabled);
    }
    public static boolean isEnabled(View view) {
        if(view == null) {
            return false;
        }
        return view.isEnabled();
    }

    public static void setVisibility(View view, int visibility) {
        if(view == null) {
            return;
        }
        view.setVisibility(visibility);
    }
}
