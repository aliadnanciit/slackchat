package com.mywork.slackchat.common;

import android.util.Log;

/**
 * Created by ali_d on 3/20/18.
 */

public class Logger {
    public static boolean LOGGER_ENABLED = true;

    public static void i(String tag, String message) {
        if(!LOGGER_ENABLED) {
            return;
        }

        Log.i(tag, message);
    }

}
