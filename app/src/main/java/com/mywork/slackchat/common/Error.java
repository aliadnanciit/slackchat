package com.mywork.slackchat.common;

public enum Error {

    ERROR_NONE(0, ""),
    ERROR_MISSING_PHONE(1, "Provide phone number"),
    ERROR_MISSING_NICK_NAME(2, "Provide your nick name"),
    ERROR_USER_ALREADY_EXISTS(3, "User already exist, please try new name"),
    ERROR_MESSAGE_USER_CREATION_FAIL(4, "Fail to create user on server. For more details please contact with support team."),
    ERROR_USER_DOES_NOT_EXISTS_IN_CHAT(5, "User does not exist in chat"),
    ERROR_USER_DOES_NOT_EXISTS(6, "User does not exist, please provide valid phone number"),

    ERROR_INVALID_USER(500, "Invalid user. For more details please contact with support team."),

    ERROR_INVALID_CHAT(600, "Invalid server error. For more details please contact with support team."),
    ERROR_INVALID_CHAT_ID(601, "Fail to load chat. For more details please contact with support team."),
    ERROR_FAIL_TO_SEND_MESSAGE(602, "Fail to send message, please try again"),

    ERROR_MESSAGE_OPERATION_FAIL(80, "Fail to fetch record from server."),


    ERROR_MISSING_CHAT_ROOM(700, "Provide chat room name"),
    ERROR_CHAT_ROOM_ALREADY_EXISTS(701, "Chat room already exist, please try new name"),
    ERROR_FAIL_TO_CREATE_CHAT_ROOM(702, "Fail to create to chat room, please try again"),
    ERROR_FAIL_TO_JOIN_CHAT_ROOM(703, "Fail to join chat room, please try again"),
    ERROR_CHAT_ROOM_NOT_EXISTS_EXISTS(704, "Chat room does not not exist, please try new name"),

            ;


    private int errorCode;
    private String errorMessage;

    public int getErrorCode() {
        return errorCode;
    }
    public String getErrorMessage() {
        return errorMessage;
    }

    Error(int errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }
}
