package com.mywork.slackchat.common;

/**
 * Created by ali_d on 3/21/18.
 */

public interface Constants {
    String REQUEST_NONE = "0";
    String REQUEST_SIGNUP_USER_TAG = "1";
    String REQUEST_LOGIN_USER_TAG = "2";
    String REQUEST_USER_HAS_ACTIVE_CHAT_TAG = "3";
    String REQUEST_USER_ADDED_TAG = "4";
    String REQUEST_USER_ACTIVE_CHAT_ADDED_TAG = "5";

    String REQUEST_CREATE_CHAT_TAG = "80";
//    String REQUEST_LOAD_CHAT_TAG = "81";
    String REQUEST_SEND_MESSAGE_TAG = "82";
    String REQUEST_MESSAGE_ADDED_TAG = "83";

    String REQUEST_CHAT_ROOM_ADDED_TAG = "100";
    String REQUEST_CREATE_CHAT_ROOM_TAG = "101";
    String REQUEST_JOIN_CHAT_ROOM_TAG = "102";

}
