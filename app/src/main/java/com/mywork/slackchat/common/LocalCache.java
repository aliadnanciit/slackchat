package com.mywork.slackchat.common;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

/**
 * Created by ali_d on 3/20/18.
 */

public class LocalCache {
    public static final String USER_CACHE = "USER_CACHE";
    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";

    Context context;

    @Inject
    public LocalCache(Application application) {
        context = application.getApplicationContext();
    }

    public String getUserId() {
        SharedPreferences mPrefs = context.getSharedPreferences(USER_CACHE, 0);
        return mPrefs.getString(USER_ID, null);
    }
    public void saveUserId(String userId) {
        SharedPreferences mPrefs = context.getSharedPreferences(USER_CACHE, 0);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(USER_ID, userId);
        editor.commit();
    }

    public String getValue(String key) {
        SharedPreferences mPrefs = context.getSharedPreferences(USER_CACHE, 0);
        return mPrefs.getString(key, null);
    }
    public void save(String key, String value) {
        SharedPreferences mPrefs = context.getSharedPreferences(USER_CACHE, 0);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(key, value);
        editor.commit();
    }
}
