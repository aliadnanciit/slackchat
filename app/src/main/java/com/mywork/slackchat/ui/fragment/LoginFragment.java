package com.mywork.slackchat.ui.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.mywork.slackchat.R;
import com.mywork.slackchat.common.Constants;
import com.mywork.slackchat.common.Error;
import com.mywork.slackchat.common.LocalCache;
import com.mywork.slackchat.common.Response;
import com.mywork.slackchat.common.util.ViewUtil;
import com.mywork.slackchat.di.Injectable;
import com.mywork.slackchat.model.User;
import com.mywork.slackchat.common.Logger;
import com.mywork.slackchat.ui.NavigationController;
import com.mywork.slackchat.viewmodel.UserViewModel;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class LoginFragment extends BaseFragment implements Injectable, TextWatcher {

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    @Inject
    DatabaseReference mDatabase;
    @Inject
    LocalCache localCache;
    @Inject
    NavigationController navigationController;

    private UserViewModel userViewModel;

    @BindView(R.id.user_id)
    EditText userId;
    @BindView(R.id.nick_name)
    EditText nickName;
    @BindView(R.id.error_message)
    TextView errorMessage;
    @BindView(R.id.btn_signup)
    TextView signupButton;
    @BindView(R.id.loading_signup)
    ProgressBar loadingSignup;
    @BindView(R.id.btn_login)
    TextView loginButton;
    @BindView(R.id.loading_login)
    ProgressBar loadingLogin;

    @Override
    protected int onLoadLayout() {
        return R.layout.fragment_login;
    }
    @Override
    protected void onViewLoad() {
        errorMessage.setText("");
        signupButton.setEnabled(false);
        loginButton.setEnabled(false);
        userId.addTextChangedListener(this);
        nickName.addTextChangedListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        userViewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(UserViewModel.class);
        userViewModel.onEvent().observe(this, new Observer<Response>() {
            @Override
            public void onChanged(@Nullable Response response) {
                if(response.getError() != null && response.getError() != Error.ERROR_NONE) {
                    // possible errors are ERROR_MISSING_PHONE, ERROR_MISSING_NICK_NAME, ERROR_USER_ALREADY_EXISTS, ERROR_MESSAGE_USER_CREATION_FAIL
                    //handle error here
                    errorMessage.setText(response.getError().getErrorMessage());
                    userId.setText("");
                    nickName.setText("");
                    userId.requestFocus();
                    Logger.i("", response.getError().getErrorMessage());

                    showRequestInprogress(false);
                    showLoginRequestInprogress(false);
                    return;
                }

                switch (response.getTag()) {
                    case Constants.REQUEST_NONE: // if response status is NONE then ignore it
                        return;
                    case Constants.REQUEST_SIGNUP_USER_TAG:
                    case Constants.REQUEST_LOGIN_USER_TAG:
                        errorMessage.setText("");

                        User user = (User) response.getData();
                        if(user == null) {
                            return;
                        }
                        localCache.saveUserId(user.getId());
                        localCache.save(LocalCache.USER_NAME, user.getNickName());

                        showRequestInprogress(false);
                        showLoginRequestInprogress(false);
                        loadAllOnlineUserUI();
                        break;
                }
            }
        });
    }

    @OnClick(R.id.btn_signup)
    public void onSignupButtonClicked() {
        showRequestInprogress(true);
        signUpUser(userId.getText().toString().trim(), nickName.getText().toString().trim());
    }
    @OnClick(R.id.btn_login)
    public void onLoginButtonClicked() {
        showLoginRequestInprogress(true);
        userViewModel.loginUser(userId.getText().toString().trim());
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }
    @Override
    public void afterTextChanged(Editable editable) {
        invalidateSignupButtonState();
        invalidateLoginButtonState();
    }

    private void invalidateSignupButtonState() {
        if(hasValidInput()) {
            ViewUtil.setEnabled(signupButton, true);
            //reset error as soon as user input user id and nick name
            errorMessage.setText("");
        } else {
            ViewUtil.setEnabled(signupButton, false);
        }
    }
    private boolean hasValidInput() {
        return userId.getText().toString().trim().length() > 0 && nickName.getText().toString().trim().length() > 0;
    }
    private void invalidateLoginButtonState() {
        if(userId.getText().toString().trim().length() > 0) {
            ViewUtil.setEnabled(loginButton, true);
            //reset error as soon as user input user id and nick name
            errorMessage.setText("");
        } else {
            ViewUtil.setEnabled(loginButton, false);
        }
    }
    public void signUpUser(String userId, String nickName) {
        userViewModel.signUpUser(userId, nickName);
    }

    private void showRequestInprogress(boolean requestStart) {
        if(requestStart) {
            ViewUtil.setVisibility(loadingSignup, View.VISIBLE);
            ViewUtil.setEnabled(signupButton, false);
        }
        else {
            ViewUtil.setVisibility(loadingSignup, View.GONE);
            invalidateSignupButtonState();
        }
    }
    private void showLoginRequestInprogress(boolean requestStart) {
        if(requestStart) {
            ViewUtil.setVisibility(loadingLogin, View.VISIBLE);
            ViewUtil.setEnabled(loginButton, false);
        }
        else {
            ViewUtil.setVisibility(loadingLogin, View.GONE);
        }
    }


    private void loadAllOnlineUserUI() {
        navigationController.navigateToHomeFragment();
    }
}
