package com.mywork.slackchat.ui.fragment;

/**
 * Created by ali_d on 3/22/18.
 */

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment extends Fragment {
    protected View rootView;

    private Unbinder unbinder;

    protected abstract int onLoadLayout();
    protected abstract void onViewLoad();

    @Nullable
    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(onLoadLayout(), container, false);
        unbinder = ButterKnife.bind(this, rootView);

        onViewLoad();

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        rootView = null;
    }
}