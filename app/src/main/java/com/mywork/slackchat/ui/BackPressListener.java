package com.mywork.slackchat.ui;

public interface BackPressListener {
    void onBackPressed();
}
