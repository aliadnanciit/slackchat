package com.mywork.slackchat.ui.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mywork.slackchat.R;
import com.mywork.slackchat.common.Constants;
import com.mywork.slackchat.common.Error;
import com.mywork.slackchat.common.LocalCache;
import com.mywork.slackchat.common.Response;
import com.mywork.slackchat.di.Injectable;
import com.mywork.slackchat.model.ActiveChat;
import com.mywork.slackchat.model.ChatRoom;
import com.mywork.slackchat.ui.NavigationController;
import com.mywork.slackchat.viewmodel.ChatRoomViewModel;
import com.mywork.slackchat.viewmodel.ChatViewModel;
import com.mywork.slackchat.viewmodel.UserViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class ChatRoomFragment extends BaseFragment implements Injectable {
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    @Inject
    LocalCache localCache;
    @Inject
    NavigationController navigationController;

    private ChatRoomViewModel chatRoomViewModel;
    private ChatRoomAdapter adapter = new ChatRoomAdapter();

    private List<ChatRoom> allOnChatRooms = new ArrayList<ChatRoom>();

    private UserViewModel userViewModel;

    private ChatViewModel chatViewModel;
    private HashMap<String, ActiveChat> activeChatsMap = new HashMap<String, ActiveChat>();


    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.info_message)
    TextView infoMessage;
    @BindView(R.id.loading)
    View loading;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.btn_create_new_chat_room)
    TextView createNewChatRoomBtn;


    @Override
    protected int onLoadLayout() {
        return R.layout.fragment_chat_rooms;
    }
    @Override
    protected void onViewLoad() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);

        recyclerView.setVisibility(View.INVISIBLE);
        loading.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.fab)
    public void createNewChatRoom() {
        showNewChatRoomCreationDialog("");
    }
    @OnClick(R.id.btn_create_new_chat_room)
    public void createNewChatRoomFromButton() {
        showNewChatRoomCreationDialog("");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // this view model is responsible to open chat room and and create new one
        userViewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(UserViewModel.class);
        userViewModel.fetchUserActiveChats(localCache.getUserId());
        userViewModel.onEvent().observe(this, new Observer<Response>() {
            @Override
            public void onChanged(@Nullable Response response) {
                if(response == null) {
                    return;
                }
                // if response status is NONE then ignore it
                if(response.getError() != Error.ERROR_NONE) {
                    //handle error here
                    return;
                }

                if(response.getTag().equals(Constants.REQUEST_NONE) &&
                        response.getTag().endsWith(Constants.REQUEST_NONE)) {
                    return;
                }

                // process event based on request type/tag
                switch (response.getTag()) {
                    case Constants.REQUEST_USER_ACTIVE_CHAT_ADDED_TAG:
                        ActiveChat activeChat = (ActiveChat) response.getData();
                        addActiveChat(activeChat);
                        break;
                    case Constants.REQUEST_JOIN_CHAT_ROOM_TAG:
                        adapter.notifyDataSetChanged();
                        break;
                }

                chatRoomViewModel.setOnEvent(null);
            }
        });

        chatViewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(ChatViewModel.class);

        // chat room view model is responsible to fetch all available chat rooms
        chatRoomViewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(ChatRoomViewModel.class);
        // request to fetch all online user
        chatRoomViewModel.fetchAllChatRooms();

        chatRoomViewModel.onEvent().observe(this, new Observer<Response>() {
            @Override
            public void onChanged(@Nullable Response response) {
                if(response == null) {
                    return;
                }
                // if response status is NONE then ignore it
                if(response.getError() != Error.ERROR_NONE) {
                    //handle error here
                    switch (response.getError()) {
                        case ERROR_MISSING_CHAT_ROOM:
                            break;
                        case ERROR_CHAT_ROOM_ALREADY_EXISTS:
                            break;
                        case ERROR_FAIL_TO_CREATE_CHAT_ROOM:
                            break;
                    }
                    return;
                }

                if(response.getTag().equals(Constants.REQUEST_NONE) &&
                        response.getTag().endsWith(Constants.REQUEST_NONE)) {
                    return;
                }


                // process event based on request type/tag
                switch (response.getTag()) {
                    case Constants.REQUEST_CREATE_CHAT_ROOM_TAG:
                        break;
                    case Constants.REQUEST_CHAT_ROOM_ADDED_TAG:
                        ChatRoom chatRoom = (ChatRoom) response.getData();
                        addChatRoom(chatRoom);
                        break;
                    case Constants.REQUEST_JOIN_CHAT_ROOM_TAG:
                        adapter.notifyDataSetChanged();
                        break;

                }

                chatRoomViewModel.setOnEvent(null);
            }
        });
    }

    public void addChatRoom(ChatRoom chatRoom) {
        if(chatRoom == null || isChatRoomExistInList(allOnChatRooms, chatRoom)) {
            if(allOnChatRooms.size() > 0) {
                recyclerView.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);
            }
            return;
        }

        allOnChatRooms.add(chatRoom);
        if(allOnChatRooms.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            loading.setVisibility(View.GONE);
        }
        adapter.notifyItemInserted(allOnChatRooms.size());
    }
    public void addActiveChat(ActiveChat activeChat) {
        if(activeChat == null || activeChatsMap.containsKey(activeChat.getChatId())) {
            return;
        }

        activeChatsMap.put(activeChat.getActiveChatId(), activeChat);

        adapter.notifyDataSetChanged();
    }
    private boolean isChatRoomExistInList(List<ChatRoom> list, ChatRoom chatRoom) {
        if(chatRoom == null) {
            return false;
        }
        for(ChatRoom u : list) {
            if(u.getId().equals(chatRoom.getId())) {
                return true;
            }
        }
        return false;
    }

    private class ChatRoomAdapter extends RecyclerView.Adapter<ChatRoomViewHolder> {
        public ChatRoomAdapter() {
        }

        @Override
        public ChatRoomViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chat_room_item, viewGroup, false);
            ChatRoomViewHolder viewHolder = new ChatRoomViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ChatRoomViewHolder holder, int position) {
            ChatRoom chatRoom = allOnChatRooms.get(position);
            holder.view.setTag(chatRoom);
            holder.name.setText(chatRoom.getName());
            holder.join.setTag(chatRoom);

            holder.join.setVisibility(View.GONE);
            if(!activeChatsMap.containsKey(chatRoom.getName())) {
                holder.join.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public int getItemCount() {
            return allOnChatRooms.size();
        }
    }

    private class ChatRoomViewHolder extends RecyclerView.ViewHolder {
        View view;
        TextView name;
        ImageView icon;
        TextView join;

        public ChatRoomViewHolder(View view) {
            super(view);
            this.view = view;
            this.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ChatRoom selectedChatRoom = (ChatRoom) view.getTag();
                    if(selectedChatRoom == null || !activeChatsMap.containsKey(selectedChatRoom.getName())) {
                        return;
                    }

                    // chat exits so handle it
                    String chatId = selectedChatRoom.getId();
                    chatViewModel.setSelectedChatId(chatId);
                    chatViewModel.setChatTitleLiveData(selectedChatRoom.getName());

                    //move to new chat activity
                    loadConversation();
                }
            });
            this.name = (TextView) view.findViewById(R.id.name);
            this.icon = (ImageView) view.findViewById(R.id.icon);
            this.join = (TextView) view.findViewById(R.id.btn_join);
            this.join.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ChatRoom chatRoom = (ChatRoom) view.getTag();
                    // send join request
                    chatRoomViewModel.joinChatRoom(localCache.getUserId(), chatRoom);
                }
            });
        }
    }

    private void loadConversation() {
        chatViewModel.setIsGroupChatLiveData(true);
        navigationController.navigateToConversationFragment();
    }

    private void showNewChatRoomCreationDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("New Chat room");
        final EditText input = new EditText(getContext());
        if(text != null && !text.isEmpty()) {
            input.setText(text);
            input.requestFocus();
            input.selectAll();
        }
        input.setBackgroundColor(Color.TRANSPARENT);
        Resources r = getResources();
        int px = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, r.getDisplayMetrics());
        input.setPadding(px, px, px, 0);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setHint("Type new chat room name");
        builder.setView(input);

        builder.setPositiveButton("Create", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String chatRoomName = input.getText().toString();
                if(!isValidName(chatRoomName)) {
                    input.selectAll();
                    input.requestFocus();

                    showMessageDialog(chatRoomName);
                    return;
                }
                String userId = localCache.getUserId();
                ChatRoom chatRoom = ChatRoom.toBuildChatRoom(userId, chatRoomName);
                chatRoomViewModel.createChatRoom(chatRoom);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private boolean isValidName(String name) {
        if(name.contains(".") || name.contains("#") || name.contains("$") || name.contains("[") || name.contains("]")) {
            return false;
        }
        return true;
    }

    private void showMessageDialog(final String text) {
        String message = "Following characters in chat room are not allowed '.', '#', '$', '[', or ']'";
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("info");
        builder.setMessage(message);
        builder.setPositiveButton("Okay, GOT IT!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                showNewChatRoomCreationDialog(text);
            }
        });
        builder.setCancelable(false);
        builder.show();
    }
}
