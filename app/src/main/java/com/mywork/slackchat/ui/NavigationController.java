package com.mywork.slackchat.ui;

import android.support.v4.app.FragmentManager;

import com.mywork.slackchat.R;
import com.mywork.slackchat.ui.activity.MainActivity;
import com.mywork.slackchat.ui.fragment.ChatRoomFragment;
import com.mywork.slackchat.ui.fragment.ConversationFragment;
import com.mywork.slackchat.ui.fragment.HomeFragment;
import com.mywork.slackchat.ui.fragment.LoginFragment;
import com.mywork.slackchat.ui.fragment.UsersFragment;

import javax.inject.Inject;

public class NavigationController {
    private final int containerId;
    private final FragmentManager fragmentManager;

    @Inject
    public NavigationController(MainActivity activity) {
        this.containerId = R.id.container;
        this.fragmentManager = activity.getSupportFragmentManager();
    }
    private void popFragment() {
        fragmentManager.popBackStack();
    }

    public void navigateToLoginFragment() {
        LoginFragment fragment = new LoginFragment();
        fragmentManager.beginTransaction()
                .replace(containerId, fragment)
                .commitAllowingStateLoss();
    }

    public void navigateToUserFragment() {
        // pop login fragment and then load new user fragment
        popFragment();

        UsersFragment fragment = new UsersFragment();
        fragmentManager.beginTransaction()
                .replace(containerId, fragment)
                .commitAllowingStateLoss();
    }

    public void navigateToConversationFragment() {
        ConversationFragment fragment = new ConversationFragment();
        fragmentManager.beginTransaction()
                .replace(containerId, fragment)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void navigateToChatRoomFragment() {
        popFragment();
        ChatRoomFragment fragment = new ChatRoomFragment();
        fragmentManager.beginTransaction()
                .replace(containerId, fragment)
                .commitAllowingStateLoss();
    }

    public void navigateToHomeFragment() {
        popFragment();
        HomeFragment fragment = new HomeFragment();
        fragmentManager.beginTransaction()
                .replace(containerId, fragment)
                .commitAllowingStateLoss();
    }
}
