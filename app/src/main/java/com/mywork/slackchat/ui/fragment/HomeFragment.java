package com.mywork.slackchat.ui.fragment;

import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mywork.slackchat.R;
import com.mywork.slackchat.common.LocalCache;
import com.mywork.slackchat.di.Injectable;
import com.mywork.slackchat.ui.AddBackPressListener;
import com.mywork.slackchat.ui.BackPressListener;
import com.mywork.slackchat.ui.NavigationController;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class HomeFragment extends Fragment implements Injectable, BackPressListener {
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    @Inject
    LocalCache localCache;
    @Inject
    NavigationController navigationController;
    private AddBackPressListener addBackPressListener;

    ViewPagerAdapter adapter;

    Toolbar toolbar;

    @Nullable
    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        toolbar = rootView.findViewById(R.id.toolbar);

        ViewPager viewPager = (ViewPager) rootView.findViewById(R.id.pager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());

        // Add Fragments to adapter one by one
        adapter.addFragment(new UsersFragment(), "Users");
        adapter.addFragment(new ChatRoomFragment(), "Channels");
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        return rootView;
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        AppCompatActivity acc = (AppCompatActivity)getActivity();
        acc.setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getContext().getColor(R.color.white));
        if (acc.getSupportActionBar() != null) {
            toolbar.setTitle(getString(R.string.app_name));

            String userName = localCache.getValue(LocalCache.USER_NAME);
            if(userName != null && !userName.isEmpty()) {
                toolbar.setSubtitle(userName);
                toolbar.setSubtitleTextColor(getContext().getColor(R.color.white));
            }
        }
    }

    // on first launch, onAttach will be called first but on next subsequent calls only onResume will be called
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof AddBackPressListener) {
            addBackPressListener = ((AddBackPressListener)context);
            addBackPressListener.addBackPressListener(this);
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        if(getActivity() != null && getActivity() instanceof AddBackPressListener) {
            addBackPressListener = ((AddBackPressListener)getActivity());
            addBackPressListener.addBackPressListener(this);
        }
    }

    // onStop will be called every time when new fragment loaded/shown but onDetach will be called at end when app is going to close only
    @Override
    public void onStop() {
        super.onStop();
        if(addBackPressListener != null) {
            addBackPressListener.addBackPressListener(null);
            addBackPressListener = null;
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();
        if(addBackPressListener != null) {
            addBackPressListener.addBackPressListener(null);
            addBackPressListener = null;
        }
    }

    @Override
    public void onBackPressed() {
        exitDialog();
    }

    private void exitDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Are you sure to quit?");
        builder.setPositiveButton("Quit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getActivity().finish();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setNeutralButton("Logout", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onLogout();
            }
        });
        builder.show();
    }
    private void onLogout() {
        localCache.save(LocalCache.USER_NAME, null);
        localCache.save(LocalCache.USER_ID, null);
        getActivity().finish();
    }
}
