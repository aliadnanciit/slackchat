package com.mywork.slackchat.ui.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mywork.slackchat.R;
import com.mywork.slackchat.common.Constants;
import com.mywork.slackchat.common.Error;
import com.mywork.slackchat.common.LocalCache;
import com.mywork.slackchat.common.Response;
import com.mywork.slackchat.di.Injectable;
import com.mywork.slackchat.model.ActiveChatRequestWrapper;
import com.mywork.slackchat.model.User;
import com.mywork.slackchat.ui.NavigationController;
import com.mywork.slackchat.viewmodel.ChatViewModel;
import com.mywork.slackchat.viewmodel.UserViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class UsersFragment extends BaseFragment implements Injectable {
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    @Inject
    LocalCache localCache;
    @Inject
    NavigationController navigationController;

    private ChatViewModel chatViewModel;
    private UserViewModel userViewModel;
    private UserAdapter adapter = new UserAdapter();

    private User currentLoggedInUser; // this is currently logged in user
    private User selectedUser = null; // this user contains info of tapped user
    private List<User> allOnlineUsers = new ArrayList<User>();


    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.info_message)
    TextView infoMessage;
    @BindView(R.id.loading)
    View loading;


    @Override
    protected int onLoadLayout() {
        return R.layout.fragment_users;
    }
    @Override
    protected void onViewLoad() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);

        recyclerView.setVisibility(View.INVISIBLE);
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // this view model is responsible to start chat and chat specific features
        chatViewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(ChatViewModel.class);

        // user view model is responsible to fetch all online users
        userViewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(UserViewModel.class);
        // request to fetch all online user
        userViewModel.fetchAllOnlineUsers();

        // observe for login user
        userViewModel.getLoginUserLiveData().observe(this, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                if(user == null) {
                    return;
                }
                currentLoggedInUser = user;
                localCache.saveUserId(user.getId());
            }
        });

        userViewModel.onEvent().observe(this, new Observer<Response>() {
            @Override
            public void onChanged(@Nullable Response response) {
                if(response == null) {
                    return;
                }
                // if response status is NONE then ignore it
                if(response.getError() != Error.ERROR_NONE) {
                    //handle error here
                    return;
                }

                if(response.getTag().endsWith(Constants.REQUEST_NONE)) {
                    return;
                }

                // process event based on request type/tag
                switch (response.getTag()) {
                    case Constants.REQUEST_CREATE_CHAT_TAG:
                        break;
                    case Constants.REQUEST_USER_ADDED_TAG:
                        User user = (User) response.getData();
                        addUser(user);
                        break;
                    case Constants.REQUEST_USER_HAS_ACTIVE_CHAT_TAG:
                        ActiveChatRequestWrapper activeChatRequestWrapper = (ActiveChatRequestWrapper) response.getData();
                        openChat(activeChatRequestWrapper);
                        break;
                }
            }
        });
    }

    private void addUser(User user) {
        if(user == null || isUserExistInList(allOnlineUsers, user)) {
            if(allOnlineUsers.size() > 0) {
                recyclerView.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);
            }
            return;
        }

        String userId = localCache.getUserId();
        if(user.getId().equals(userId)) {
            userViewModel.setLoginUserLiveData(user);
        }
        else {
            allOnlineUsers.add(user);
        }
        if(allOnlineUsers.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            loading.setVisibility(View.GONE);
        }
        adapter.notifyItemInserted(allOnlineUsers.size());
    }

    private boolean isUserExistInList(List<User> users, User user) {
        if(user == null) {
            return false;
        }
        for(User u : users) {
            if(u.getId().equals(user.getId())) {
                return true;
            }
        }
        return false;
    }

    private class UserAdapter extends RecyclerView.Adapter<UserViewHolder> {
        public UserAdapter() {
        }

        @Override
        public UserViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.user_item, viewGroup, false);
            UserViewHolder viewHolder = new UserViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(UserViewHolder holder, int position) {
            User user = allOnlineUsers.get(position);
            holder.view.setTag(user);
            holder.name.setText(user.getNickName());
        }

        @Override
        public int getItemCount() {
            return allOnlineUsers.size();
        }
    }

    private class UserViewHolder extends RecyclerView.ViewHolder {
        View view;
        TextView name;
        ImageView icon;

        public UserViewHolder(View view) {
            super(view);
            this.view = view;
            this.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedUser = (User) view.getTag();
                    if(User.isEqual(selectedUser, currentLoggedInUser)) {
                        return;
                    }
                    userViewModel.openChat(currentLoggedInUser, selectedUser);
                }
            });
            this.name = (TextView) view.findViewById(R.id.name);
            this.icon = (ImageView) view.findViewById(R.id.icon);
        }
    }

    private void loadConversation() {
        chatViewModel.setIsGroupChatLiveData(false);
        navigationController.navigateToConversationFragment();
    }
    private void openChat(ActiveChatRequestWrapper activeChatRequestWrapper) {
        if(activeChatRequestWrapper == null) {
            return;
        }
        if(activeChatRequestWrapper.getActiveChat() != null) {
            userViewModel.setOnEvent(null);

            // chat exits so handle it
            String chatId = activeChatRequestWrapper.getActiveChat().getChatId();
            chatViewModel.setSelectedChatId(chatId);
            chatViewModel.setChatTitleLiveData(selectedUser.getNickName());

            //move to new chat activity
            loadConversation();
            return;
        }
        chatViewModel.createChat(activeChatRequestWrapper);
    }
}
