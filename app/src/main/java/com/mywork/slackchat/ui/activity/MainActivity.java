package com.mywork.slackchat.ui.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.mywork.slackchat.R;
import com.mywork.slackchat.common.LocalCache;
import com.mywork.slackchat.ui.AddBackPressListener;
import com.mywork.slackchat.ui.BackPressListener;
import com.mywork.slackchat.ui.NavigationController;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class MainActivity extends AppCompatActivity implements HasSupportFragmentInjector, AddBackPressListener {
    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;
    @Inject
    LocalCache localCache;
    @Inject
    NavigationController navigationController;

    private BackPressListener backPressListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String loggedInUserId = localCache.getUserId();
        if(loggedInUserId != null) {
            navigationController.navigateToHomeFragment();
            return;
        }

        // load login UI
        navigationController.navigateToLoginFragment();
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }

    public void addBackPressListener(BackPressListener backPressListener) {
        this.backPressListener = backPressListener;
    }

    @Override
    public void onBackPressed() {
        if(backPressListener != null) {
            backPressListener.onBackPressed();
            return;
        }
        super.onBackPressed();
    }
}