package com.mywork.slackchat.ui.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.mywork.slackchat.R;
import com.mywork.slackchat.common.Constants;
import com.mywork.slackchat.common.Error;
import com.mywork.slackchat.common.LocalCache;
import com.mywork.slackchat.common.Response;
import com.mywork.slackchat.di.Injectable;
import com.mywork.slackchat.model.Message;
import com.mywork.slackchat.model.User;
import com.mywork.slackchat.viewmodel.ChatViewModel;
import com.mywork.slackchat.viewmodel.UserViewModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class ConversationFragment extends BaseFragment implements Injectable {

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    @Inject
    DatabaseReference mDatabase;
    @Inject
    LocalCache localCache;

    private ChatViewModel chatViewModel;
    private UserViewModel userViewModel;
    private ChatAdapter adapter = new ChatAdapter();

    private String logedinUserId;
    private List<Message> messagesList = new ArrayList<Message>();
    private HashMap<String, User> userHashMap = new HashMap<String, User>();


    @BindView(R.id.toolbar)
    Toolbar toolBar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.message)
    EditText message;
    @BindView(R.id.send)
    ImageView send;


    @Override
    protected int onLoadLayout() {
        return R.layout.fragment_conversation;
    }
    @Override
    protected void onViewLoad() {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
//        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);
        message.setText("");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        AppCompatActivity acc = (AppCompatActivity)getActivity();
        acc.setSupportActionBar(toolBar);
        toolBar.setTitleTextColor(getContext().getColor(R.color.white));
        if (acc.getSupportActionBar() != null) {
            toolBar.setTitle("");
        }

        logedinUserId = localCache.getUserId();
        userViewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(UserViewModel.class);
        // request to fetch all online user
        userViewModel.fetchAllOnlineUsers();
        userViewModel.getLoginUserLiveData().observe(this, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                if(user == null) {
                    return;
                }
                logedinUserId = user.getId();
            }
        });
        userViewModel.onEvent().observe(this, new Observer<Response>() {
            @Override
            public void onChanged(@Nullable Response response) {
                if(response == null) {
                    return;
                }
                // if response status is NONE then ignore it
                if(response.getError() != Error.ERROR_NONE) {
                    //handle error here
                    return;
                }

                if(response.getTag().endsWith(Constants.REQUEST_NONE)) {
                    return;
                }


                // process event based on request type/tag
                switch (response.getTag()) {
                    case Constants.REQUEST_USER_ADDED_TAG:
                        User user = (User) response.getData();
                        addUser(user);
                        break;
                }
            }
        });

        chatViewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(ChatViewModel.class);
        chatViewModel.loadChat(chatViewModel.getSelectedChatId().getValue());
        chatViewModel.getRequestSentLiveData().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String requestTag) {
                if(requestTag.equals(Constants.REQUEST_NONE)) {
                    return;
                }
                switch (requestTag) {
                    case Constants.REQUEST_SEND_MESSAGE_TAG:
                        message.setText("");
                        break;
                }
                chatViewModel.setRequestSentLiveData(Constants.REQUEST_NONE);
            }
        });
        chatViewModel.getMessageAdded().observe(this, new Observer<Message>() {
            @Override
            public void onChanged(@Nullable Message message) {
                onAddMessages(message);
            }
        });

        chatViewModel.getChatTitleLiveData().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String chatTitle) {
                toolBar.setTitle(chatTitle);
            }
        });
    }

    @OnClick(R.id.send)
    public void onSendMessage() {
        Date d = new Date();
        String msg = message.getText().toString().trim();
        Message m = Message.toMessage(logedinUserId, chatViewModel.getSelectedChatId().getValue(), msg, System.currentTimeMillis());
        chatViewModel.sendMessage(chatViewModel.getSelectedChatId().getValue(), m);
    }

    private class ChatAdapter extends RecyclerView.Adapter<ChatViewHolder> {

        public ChatAdapter() {
        }

        @Override
        public ChatViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.message_item, viewGroup, false);
            ChatViewHolder viewHolder = new ChatViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ChatViewHolder holder, int position) {
            Message message = messagesList.get(position);
            holder.view.setTag(message);
            holder.message.setText(message.getMessage());
            String time = new SimpleDateFormat("HH:mm").format((message.getTime()));

            ((LinearLayout)holder.view).setGravity(Gravity.LEFT);
            if(logedinUserId.equals(message.getFrom())) {
                ((LinearLayout)holder.view).setGravity(Gravity.RIGHT);
            }
            else if(chatViewModel.getIsGroupChatLiveData().getValue()){
                User user = userHashMap.get(message.getFrom());
                if(user != null) {
                    time = "By "+ user.getNickName() +" at "+time;
                }
            }
            holder.time.setText(time);
        }

        @Override
        public int getItemCount() {
            return messagesList.size();
        }
    }

    private class ChatViewHolder extends RecyclerView.ViewHolder {
        View view;
        TextView message;
        TextView time;

        public ChatViewHolder(View view) {
            super(view);
            this.view = view;
            this.message = (TextView) view.findViewById(R.id.message);
            this.time = (TextView) view.findViewById(R.id.time);
        }
    }

    private void onAddMessages(Message message) {
        if(message == null || ignoreMessage(messagesList, message)) {
            return;
        }
        messagesList.add(message);
        adapter.notifyItemInserted(messagesList.size());
        recyclerView.smoothScrollToPosition(messagesList.size());
    }
    private boolean ignoreMessage(List<Message> messages, Message message) {
        if(message == null) {
            return false;
        }
        // ignore this message if it is already in message list
        for(Message m : messages) {
            if(m.getTime() == message.getTime() && m.getFrom().equals(message.getFrom())) {
                return true;
            }
        }

        // ignore this message if message do not belongs to chat id (chat id can be user or groups chat id)
        if(!message.getTo().equals(chatViewModel.getSelectedChatId().getValue())) {
            return true;
        }

        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        chatViewModel.setMessageAdded(null);
    }

    public void addUser(User user) {
        if(user == null || userHashMap.containsKey(user.getId())) {
            return;
        }
        userHashMap.put(user.getId(), user);
    }
}
