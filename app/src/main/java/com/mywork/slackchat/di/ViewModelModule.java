package com.mywork.slackchat.di;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.mywork.slackchat.viewmodel.ChatRoomViewModel;
import com.mywork.slackchat.viewmodel.ChatViewModel;
import com.mywork.slackchat.viewmodel.UserViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ChatViewModel.class)
    abstract ViewModel bindChatViewModel(ChatViewModel chatViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(UserViewModel.class)
    abstract ViewModel bindLoginViewModel(UserViewModel userViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ChatRoomViewModel.class)
    abstract ViewModel bindChatRoomViewModel(ChatRoomViewModel chatRoomViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(SlackViewModelFactory factory);
}
