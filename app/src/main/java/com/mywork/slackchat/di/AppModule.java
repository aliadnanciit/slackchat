package com.mywork.slackchat.di;

import android.app.Application;
import android.content.Context;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mywork.slackchat.common.LocalCache;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = ViewModelModule.class)
public class AppModule {

    @Provides
    @Singleton
    Context provideContext(Application application) {
        return application;
    }

    @Provides
    @Singleton
    DatabaseReference provideDatabase() {
        return FirebaseDatabase.getInstance().getReference();
    }

    @Provides
    @Singleton
    LocalCache provideLocalCache(Application application) {
        return new LocalCache(application);
    }

}
