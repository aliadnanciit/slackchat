package com.mywork.slackchat.di;

/**
 * Marks an activity / fragment injectable.
 */
public interface Injectable {
}