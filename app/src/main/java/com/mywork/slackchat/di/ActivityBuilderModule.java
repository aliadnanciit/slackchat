package com.mywork.slackchat.di;

import com.mywork.slackchat.ui.activity.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilderModule {
    @ContributesAndroidInjector(modules = {ChatsFragmentBuilderModule.class})
    abstract MainActivity contributeLoginActivity();

}
