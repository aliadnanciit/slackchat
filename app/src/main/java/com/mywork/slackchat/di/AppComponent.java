package com.mywork.slackchat.di;

import android.app.Application;

import com.mywork.slackchat.SlackApplication;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {AndroidInjectionModule.class, AppModule.class, ActivityBuilderModule.class})
public interface AppComponent {

    // creating our own custom builder to bind application instance with dagger objects graph
    @Component.Builder
    interface Builder {
        @BindsInstance Builder application(Application application);
        AppComponent build();
    }

    void inject(SlackApplication app);
}
