package com.mywork.slackchat.di;

import com.mywork.slackchat.ui.fragment.ChatRoomFragment;
import com.mywork.slackchat.ui.fragment.ConversationFragment;
import com.mywork.slackchat.ui.fragment.HomeFragment;
import com.mywork.slackchat.ui.fragment.LoginFragment;
import com.mywork.slackchat.ui.fragment.UsersFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ChatsFragmentBuilderModule {

    @ContributesAndroidInjector
    abstract LoginFragment contributeLoginFragment();

    @ContributesAndroidInjector
    abstract UsersFragment contributeUserFragment();

    @ContributesAndroidInjector
    abstract ConversationFragment contributeConversationFragment();

    @ContributesAndroidInjector
    abstract ChatRoomFragment contributeChatRoomFragment();

    @ContributesAndroidInjector
    abstract HomeFragment contributeHomeFragment();
}
