package com.mywork.slackchat.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.mywork.slackchat.common.Constants;
import com.mywork.slackchat.common.Error;
import com.mywork.slackchat.common.Response;
import com.mywork.slackchat.model.User;
import com.mywork.slackchat.repository.UserRepository;
import com.mywork.slackchat.services.RequestCallback;

import javax.inject.Inject;

public class UserViewModel extends ViewModel implements RequestCallback {
    private MutableLiveData<User> loginUserLiveData = new MutableLiveData<>();
    private MutableLiveData<Response> onEvent = new MutableLiveData<>();

    private UserRepository userRepository;

    @Inject
    public UserViewModel(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void loginUser(String userId) {
        userRepository.loginUser(this, userId);
    }

    public void signUpUser(String userId, String nickName) {
        userRepository.signUpUser(this, userId, nickName);
    }
    public void fetchAllOnlineUsers() {
        userRepository.fetchAllUsers(this);
    }
    public void fetchUserActiveChats(String userId) {
        userRepository.fetchUserActiveChats(this, userId);
    }
    public void openChat(User loginUser, User friend) {
        userRepository.openChat(this, loginUser, friend);
    }

    @Override
    public void onResult(Response response) {
        if(response.getError() != Error.ERROR_NONE) {
            return;
        }

        switch (response.getTag()) {
            case Constants.REQUEST_SIGNUP_USER_TAG:
            case Constants.REQUEST_LOGIN_USER_TAG:
                User user = (User) response.getData();
                loginUserLiveData.setValue(user);
                setOnEvent(response);
                break;
            case Constants.REQUEST_USER_ADDED_TAG:
            case Constants.REQUEST_USER_HAS_ACTIVE_CHAT_TAG:
            case Constants.REQUEST_CHAT_ROOM_ADDED_TAG:
            case Constants.REQUEST_USER_ACTIVE_CHAT_ADDED_TAG:
                setOnEvent(response);
                break;
        }
    }

    public MutableLiveData<User> getLoginUserLiveData() {
        return loginUserLiveData;
    }
    public void setLoginUserLiveData(User loginUser) {
        loginUserLiveData.setValue(loginUser);
    }

    public MutableLiveData<Response> onEvent() {
        return onEvent;
    }

    public void setOnEvent(Response response) {
        onEvent.setValue(response);
    }
}
