package com.mywork.slackchat.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.mywork.slackchat.common.Constants;
import com.mywork.slackchat.common.Error;
import com.mywork.slackchat.common.Logger;
import com.mywork.slackchat.common.Response;
import com.mywork.slackchat.model.ActiveChatRequestWrapper;
import com.mywork.slackchat.model.Message;
import com.mywork.slackchat.repository.ChatRepository;
import com.mywork.slackchat.services.RequestCallback;

import javax.inject.Inject;

public class ChatViewModel extends ViewModel implements RequestCallback {
    private ChatRepository chatRepository;

    private MutableLiveData<String> requestSentLiveData = new MutableLiveData<>();
    private MutableLiveData<String> selectedChatId = new MutableLiveData<>();
    private MutableLiveData<Message> messageAdded = new MutableLiveData<>();
    private MutableLiveData<String> chatTitleLiveData = new MutableLiveData<>();
    private MutableLiveData<Boolean> isGroupChatLiveData = new MutableLiveData<>();

    @Inject
    public ChatViewModel(ChatRepository chatRepository) {
        this.chatRepository = chatRepository;

        isGroupChatLiveData.setValue(false);
    }

    public void createChat(ActiveChatRequestWrapper activeChatRequestWrapper) {
        chatRepository.createChat(this, activeChatRequestWrapper);
    }
    public void loadChat(String chatId) {
        chatRepository.loadChat(this, chatId);
    }
    public void sendMessage(String chatId, Message message) {
        chatRepository.sendMessage(this, chatId, message);
    }

    @Override
    public void onResult(Response response) {
        if(response.getError() != Error.ERROR_NONE) {
            // find error
            return;
        }

        switch (response.getTag()) {
            case Constants.REQUEST_CREATE_CHAT_TAG:
                Logger.i("", "Done");
                ActiveChatRequestWrapper activeChatRequestWrapper = (ActiveChatRequestWrapper) response.getData();
                if(activeChatRequestWrapper.getActiveChat() != null) {
                    // chat exits so handle it
                    String chatId = activeChatRequestWrapper.getActiveChat().getChatId();
                    loadChat(chatId);
                    return;
                }
                break;
            case Constants.REQUEST_SEND_MESSAGE_TAG:
                requestSentLiveData.setValue(Constants.REQUEST_SEND_MESSAGE_TAG);
                break;
            case Constants.REQUEST_MESSAGE_ADDED_TAG:
                Message m = (Message) response.getData();
                messageAdded.setValue(m);
                break;
        }
    }

    public MutableLiveData<String> getSelectedChatId() {
        return selectedChatId;
    }
    public void setSelectedChatId(String chatId) {
        selectedChatId.setValue(chatId);
    }

    public MutableLiveData<String> getRequestSentLiveData() {
        return requestSentLiveData;
    }
    public void setRequestSentLiveData(String requestTag) {
        requestSentLiveData.setValue(requestTag);
    }

    public MutableLiveData<Message> getMessageAdded() {
        return messageAdded;
    }

    public void setMessageAdded(Message message) {
        messageAdded.setValue(message);
    }

    public MutableLiveData<String> getChatTitleLiveData() {
        return chatTitleLiveData;
    }
    public void setChatTitleLiveData(String title) {
        this.chatTitleLiveData.setValue(title);
    }

    public MutableLiveData<Boolean> getIsGroupChatLiveData() {
        return isGroupChatLiveData;
    }
    public void setIsGroupChatLiveData(boolean isGroupChat) {
        isGroupChatLiveData.setValue(isGroupChat);
    }
}
