package com.mywork.slackchat.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.mywork.slackchat.common.Constants;
import com.mywork.slackchat.common.Error;
import com.mywork.slackchat.common.Response;
import com.mywork.slackchat.model.ChatRoom;
import com.mywork.slackchat.repository.ChatRoomRepository;
import com.mywork.slackchat.services.RequestCallback;

import javax.inject.Inject;

public class ChatRoomViewModel extends ViewModel implements RequestCallback {
    private MutableLiveData<Response> onEvent = new MutableLiveData<>();

    private ChatRoomRepository chatRoomRepository;

    @Inject
    public ChatRoomViewModel(ChatRoomRepository chatRoomRepository) {
        this.chatRoomRepository = chatRoomRepository;
    }

    public void createChatRoom(ChatRoom chatRoom) {
        chatRoomRepository.createChatRoom(this, chatRoom);
    }
    public void joinChatRoom(String userId, ChatRoom chatRoom) {
        chatRoomRepository.joinChatRoom(this, userId, chatRoom);
    }

    public void fetchAllChatRooms() {
        chatRoomRepository.fetchAllChatRooms(this);
    }

    @Override
    public void onResult(Response response) {
        if(response.getError() != Error.ERROR_NONE) {
            setOnEvent(Response.toError(Constants.REQUEST_NONE, response.getError()));
            return;
        }

        switch (response.getTag()) {
            case Constants.REQUEST_CHAT_ROOM_ADDED_TAG:
            case Constants.REQUEST_CREATE_CHAT_ROOM_TAG:
            case Constants.REQUEST_JOIN_CHAT_ROOM_TAG:
                setOnEvent(response);
                break;
        }
    }

    public MutableLiveData<Response> onEvent() {
        return onEvent;
    }
    public void setOnEvent(Response response) {
        onEvent.setValue(response);
    }
}
