package com.mywork.slackchat.services;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mywork.slackchat.common.Constants;
import com.mywork.slackchat.common.Error;
import com.mywork.slackchat.common.Response;
import com.mywork.slackchat.model.ActiveChat;
import com.mywork.slackchat.model.ChatRoom;
import com.mywork.slackchat.model.Message;
import com.mywork.slackchat.common.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.inject.Inject;

/**
 * Created by ali_d on 3/20/18.
 */

public class ChatRoomService {
    public static final String TAG = "CHAT_ROOM";
    public static final String CHAT_ROOMS = "chatRooms";

    @Inject
    DatabaseReference databaseReference;

    private RequestCallback requestCallback;

    @Inject
    public ChatRoomService() {
    }

    public void createChatRoom(RequestCallback requestCallback, final ChatRoom chatRoom) {
        this.requestCallback = requestCallback;

        if(chatRoom.getName() == null || chatRoom.getName().isEmpty()) {
            onResult(Response.toError(Constants.REQUEST_CREATE_CHAT_ROOM_TAG, Error.ERROR_MISSING_CHAT_ROOM));
            return;
        }

        Query query = databaseReference.child(CHAT_ROOMS).child(chatRoom.getName());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    onResult(Response.toError(Constants.REQUEST_CREATE_CHAT_ROOM_TAG, Error.ERROR_CHAT_ROOM_ALREADY_EXISTS));
                } else {
                    Map<String, Object> childUpdates = new HashMap<>();

                    // create chat entry under /chats for room chat first
                    String chatIdForChatRoom = databaseReference.child(ChatService.CHATS).push().getKey();
                    childUpdates.put("/"+ChatService.CHATS + "/"+chatIdForChatRoom, new ArrayList<Message>());

                    // add chat room new entry under /chatRooms
                    ChatRoom ch = ChatRoom.toBuildChatRoom(chatRoom);
                    ch.setId(chatIdForChatRoom);
                    childUpdates.put("/"+CHAT_ROOMS + "/"+chatRoom.getName(), ch);

                    // add newly created chat room entry into all members active chat chats under /users/user/activechats
                    HashMap<String, Boolean> members = chatRoom.getMembers();
                    String path, userId;
                    Iterator<String> userIds = members.keySet().iterator();
                    ActiveChat activeChat = ActiveChat.buildGroupChat(chatIdForChatRoom, 0, true);
                    while(userIds.hasNext()) {
                        userId = userIds.next();
                        // add chat room entry into user active chat as well
                        path = "/"+UserService.USERS+"/"+userId+"/"+UserService.ACTIVE_CHATS;
                        childUpdates.put(path +"/"+ chatRoom.getName(), activeChat);
                    }

                    // save all data at once
                    databaseReference.updateChildren(childUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()) {
                                onResult(Response.toSuccess(Constants.REQUEST_CREATE_CHAT_ROOM_TAG, chatRoom));
                            }
                            else {
                                onResult(Response.toError(Constants.REQUEST_CREATE_CHAT_ROOM_TAG, Error.ERROR_FAIL_TO_CREATE_CHAT_ROOM));
                            }
                        }
                    });
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                onResult(Response.toError(Constants.REQUEST_SIGNUP_USER_TAG, Error.ERROR_MESSAGE_USER_CREATION_FAIL));
            }
        });
    }

    public void joinChatRoom(RequestCallback requestCallback, final String userId, final ChatRoom chatRoom) {
        this.requestCallback = requestCallback;

        if(chatRoom.getName() == null || chatRoom.getName().isEmpty()) {
            onResult(Response.toError(Constants.REQUEST_CREATE_CHAT_ROOM_TAG, Error.ERROR_MISSING_CHAT_ROOM));
            return;
        }

        Query query = databaseReference.child(CHAT_ROOMS).child(chatRoom.getName());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    ChatRoom chatRoom1 = dataSnapshot.getValue(ChatRoom.class);
                    chatRoom1.getMembers().put(userId, true);

                    Map<String, Object> childUpdates = new HashMap<>();
                    childUpdates.put("/"+CHAT_ROOMS + "/"+chatRoom.getName(), chatRoom1);

                    ActiveChat activeChat = ActiveChat.buildGroupChat(chatRoom1.getId(), 0, true);
                    String path = "/"+UserService.USERS+"/"+userId+"/"+UserService.ACTIVE_CHATS;
                    childUpdates.put(path +"/"+ chatRoom1.getName(), activeChat); // chatRoom1.getName() is chat rom name (key)

                    // save all data at once
                    databaseReference.updateChildren(childUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()) {
                                onResult(Response.toSuccess(Constants.REQUEST_JOIN_CHAT_ROOM_TAG, chatRoom));
                            }
                            else {
                                onResult(Response.toError(Constants.REQUEST_JOIN_CHAT_ROOM_TAG, Error.ERROR_FAIL_TO_JOIN_CHAT_ROOM));
                            }
                        }
                    });
                } else {
                    onResult(Response.toError(Constants.REQUEST_JOIN_CHAT_ROOM_TAG, Error.ERROR_CHAT_ROOM_NOT_EXISTS_EXISTS));
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                onResult(Response.toError(Constants.REQUEST_JOIN_CHAT_ROOM_TAG, Error.ERROR_CHAT_ROOM_NOT_EXISTS_EXISTS));
            }
        });
    }

    public void fetchAllChatRooms(final RequestCallback requestCallback) {
        this.requestCallback = requestCallback;
        databaseReference.child(CHAT_ROOMS).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                ChatRoom chatRoom = dataSnapshot.getValue(ChatRoom.class);
                onResult(Response.toSuccess(Constants.REQUEST_CHAT_ROOM_ADDED_TAG, chatRoom));
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void onResult(Response response) {
        if(response != null && response.getError() != null) {
            Logger.i(TAG, response.getError().getErrorMessage());
        }

        if(requestCallback == null) {
            return;
        }
        requestCallback.onResult(response);
    }
}
