package com.mywork.slackchat.services;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.mywork.slackchat.common.Constants;
import com.mywork.slackchat.common.Error;
import com.mywork.slackchat.common.Logger;
import com.mywork.slackchat.common.Response;
import com.mywork.slackchat.model.ActiveChat;
import com.mywork.slackchat.model.ActiveChatRequestWrapper;
import com.mywork.slackchat.model.Message;
import com.mywork.slackchat.model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

/**
 * Created by ali_d on 3/20/18.
 */

public class ChatService {
    public static final String TAG = "CHATS";
    public static final String CHATS = "chats";

    @Inject
    DatabaseReference databaseReference;

    private RequestCallback requestCallback;

    @Inject
    public ChatService() {
    }

    public void createChat(RequestCallback requestCallback, final ActiveChatRequestWrapper activeChatRequestWrapper) {
        this.requestCallback = requestCallback;

        Map<String, Object> childUpdates = new HashMap<>();

        String chatId = databaseReference.child(CHATS).push().getKey();
        childUpdates.put("/"+CHATS + "/"+chatId, new ArrayList<Message>());

        String path;
        ActiveChat activeChat = ActiveChat.buildGroupChat(chatId, 0, false);
        ArrayList<User> users = (ArrayList<User>)activeChatRequestWrapper.getData();
        User u1 = users.get(0);
        User u2 = users.get(1);

        path = "/"+UserService.USERS+"/"+u1.getId()+"/"+UserService.ACTIVE_CHATS;
        childUpdates.put(path +"/"+ u2.getId(), activeChat);

        path = "/"+UserService.USERS+"/"+u2.getId()+"/"+UserService.ACTIVE_CHATS;
        childUpdates.put(path +"/"+ u1.getId(), activeChat);

        final ActiveChatRequestWrapper activeChatRequestWrapper1 =
                ActiveChatRequestWrapper.buildUserChat(activeChat, activeChatRequestWrapper.getData());

        databaseReference.updateChildren(childUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()) {
                    onResult(Response.toSuccess(Constants.REQUEST_CREATE_CHAT_TAG, activeChatRequestWrapper1));
                }
                else {
                    onResult(Response.toError(Constants.REQUEST_CREATE_CHAT_TAG, Error.ERROR_INVALID_CHAT));
                }

            }
        });
    }

    public void loadChat(RequestCallback requestCallback, final String chatId) {
        this.requestCallback = requestCallback;

        if(chatId == null || chatId.isEmpty()) {
            onResult(Response.toError(Constants.REQUEST_MESSAGE_ADDED_TAG, Error.ERROR_INVALID_CHAT_ID));
            return;
        }

        databaseReference.child(CHATS).child(chatId).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                Message message = dataSnapshot.getValue(Message.class);
                onResult(Response.toSuccess(Constants.REQUEST_MESSAGE_ADDED_TAG, message));
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

    public void sendMessage(RequestCallback requestCallback, String chatId, final Message message) {
        this.requestCallback = requestCallback;

        Map<String, Object> childUpdates = new HashMap<>();
        String key = databaseReference.child(CHATS).child(chatId).push().getKey();
        childUpdates.put("/"+CHATS + "/"+chatId+"/"+key, message);
        databaseReference.updateChildren(childUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()) {
                    onResult(Response.toSuccess(Constants.REQUEST_SEND_MESSAGE_TAG, message));
                }
                else {
                    onResult(Response.toError(Constants.REQUEST_SEND_MESSAGE_TAG, Error.ERROR_FAIL_TO_SEND_MESSAGE));
                }
            }
        });
    }

    private void onResult(Response response) {
        if(response != null && response.getError() != null) {
            Logger.i(TAG, response.getError().getErrorMessage());
        }

        if(requestCallback == null) {
            return;
        }
        requestCallback.onResult(response);
    }
}
