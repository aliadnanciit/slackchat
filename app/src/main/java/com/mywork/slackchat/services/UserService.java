package com.mywork.slackchat.services;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mywork.slackchat.common.Constants;
import com.mywork.slackchat.common.Error;
import com.mywork.slackchat.common.Response;
import com.mywork.slackchat.model.ActiveChat;
import com.mywork.slackchat.model.ActiveChatRequestWrapper;
import com.mywork.slackchat.model.User;
import com.mywork.slackchat.common.Logger;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by ali_d on 3/20/18.
 */

public class UserService {
    public static final String TAG = "USER";
    public static final String USERS = "users";
    public static final String ACTIVE_CHATS = "activeChats";

    @Inject
    DatabaseReference databaseReference;

    private RequestCallback requestCallback;

    @Inject
    public UserService() {
    }

    public void loginUser(final RequestCallback requestCallback, final String userId) {
        this.requestCallback = requestCallback;
        // generate error if nick name is null or empty
        if(userId == null || userId.isEmpty()) {
            onResult(Response.toError(Constants.REQUEST_LOGIN_USER_TAG, Error.ERROR_MISSING_PHONE));
            return;
        }
        Query query = databaseReference.child(USERS).child(userId);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    User user = dataSnapshot.getValue(User.class);
                    onResult(Response.toSuccess(Constants.REQUEST_LOGIN_USER_TAG, user));
                } else {
                    onResult(Response.toError(Constants.REQUEST_LOGIN_USER_TAG, Error.ERROR_USER_DOES_NOT_EXISTS));
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                onResult(Response.toError(Constants.REQUEST_LOGIN_USER_TAG, Error.ERROR_USER_DOES_NOT_EXISTS));
            }
        });
    }
    public void signUpUser(final RequestCallback requestCallback, final String userId, final String nickName) {
        this.requestCallback = requestCallback;
        // generate error if nick name is null or empty
        if(userId == null || userId.isEmpty()) {
            onResult(Response.toError(Constants.REQUEST_SIGNUP_USER_TAG, Error.ERROR_MISSING_PHONE));
            return;
        }
        if(nickName == null || nickName.isEmpty()) {
            onResult(Response.toError(Constants.REQUEST_SIGNUP_USER_TAG, Error.ERROR_MISSING_NICK_NAME));
            return;
        }

        Query query = databaseReference.child(USERS).child(userId);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User uu = dataSnapshot.getValue(User.class);
                if (dataSnapshot.exists()) {
                    onResult(Response.toError(Constants.REQUEST_SIGNUP_USER_TAG, Error.ERROR_USER_ALREADY_EXISTS));
                } else {
                    User user = User.toUser(userId, nickName, null);
                    databaseReference.child(USERS).child(userId).setValue(user)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()) {
                                onResult(Response.toSuccess(Constants.REQUEST_SIGNUP_USER_TAG, User.toUser(userId, nickName)));
                            }
                            else {
                                onResult(Response.toError(Constants.REQUEST_SIGNUP_USER_TAG, Error.ERROR_MESSAGE_USER_CREATION_FAIL));
                            }
                        }
                    });
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                onResult(Response.toError(Constants.REQUEST_SIGNUP_USER_TAG, Error.ERROR_MESSAGE_USER_CREATION_FAIL));
            }
        });
    }
    public void fetchAllUsers(final RequestCallback requestCallback) {
        this.requestCallback = requestCallback;
        databaseReference.child(USERS).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                User user = dataSnapshot.getValue(User.class);
                onResult(Response.toSuccess(Constants.REQUEST_USER_ADDED_TAG, user));
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    public void fetchUserActiveChats(final RequestCallback requestCallback, String userId) {
        this.requestCallback = requestCallback;
        databaseReference.child(USERS).child(userId).child(ACTIVE_CHATS).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                String key = dataSnapshot.getKey();
                ActiveChat activeChat = dataSnapshot.getValue(ActiveChat.class);
                if(activeChat != null) {
                    activeChat.setActiveChatId(key);
                }
                onResult(Response.toSuccess(Constants.REQUEST_USER_ACTIVE_CHAT_ADDED_TAG, activeChat));
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }


    public void openChat(RequestCallback requestCallback, User loginUser, User friend) {
        List<User> users = new ArrayList<User>(2);
        users.add(loginUser);
        users.add(friend);
        ActiveChatRequestWrapper activeChatRequestWrapper = ActiveChatRequestWrapper.buildUserChat(null, users);
        findUserActiveChat(requestCallback, activeChatRequestWrapper);
    }

    public void findUserActiveChat(RequestCallback requestCallback, final ActiveChatRequestWrapper activeChatRequestWrapper) {
        this.requestCallback = requestCallback;
        if(activeChatRequestWrapper == null || activeChatRequestWrapper.getData() == null) {
            onResult(Response.toError(Constants.REQUEST_USER_HAS_ACTIVE_CHAT_TAG, Error.ERROR_INVALID_USER));
            return;
        }
        ArrayList<User> users = (ArrayList<User>) activeChatRequestWrapper.getData();
        User loginUser = users.get(0);
        User friend = users.get(1);

        Query query = databaseReference.child(USERS).child(loginUser.getId()).child(ACTIVE_CHATS).child(friend.getId());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ActiveChat activeChat = dataSnapshot.getValue(ActiveChat.class);
                onResult(Response.toSuccess(Constants.REQUEST_USER_HAS_ACTIVE_CHAT_TAG, ActiveChatRequestWrapper.buildUserChat(activeChat, activeChatRequestWrapper.getData())));
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                onResult(Response.toError(Constants.REQUEST_USER_HAS_ACTIVE_CHAT_TAG, Error.ERROR_MESSAGE_USER_CREATION_FAIL));
            }
        });
    }
    private void onResult(Response response) {
        if(response != null && response.getError() != null) {
            Logger.i(TAG, response.getError().getErrorMessage());
        }

        if(requestCallback == null) {
            return;
        }
        requestCallback.onResult(response);
    }
}
