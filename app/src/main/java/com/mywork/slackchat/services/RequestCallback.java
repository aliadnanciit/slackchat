package com.mywork.slackchat.services;

import com.mywork.slackchat.common.Response;

/**
 * Created by ali_d on 3/20/18.
 */

public interface RequestCallback {
    void onResult(Response response);
}
