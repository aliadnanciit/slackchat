package com.mywork.slackchat.model;

/**
 * Created by ali_d on 3/20/18.
 */

public class Message {
    private String from;
    private String to;
    String message;
    long time;

    private Message() {
    }
    private Message(String from, String to, String message, long time) {
        this.from = from;
        this.to = to;
        this.message = message;
        this.time = time;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }
    public void setTo(String to) {
        this.to = to;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public static Message toMessage(String from, String to, String message, long time) {
        return new Message(from, to, message, time);
    }
}
