package com.mywork.slackchat.model;

/**
 * Created by ali_d on 3/23/18.
 */

public class ActiveChatRequestWrapper {
    ActiveChat activeChat;
    Object data;

    private ActiveChatRequestWrapper(ActiveChat activeChat, Object data) {
        this.activeChat = activeChat;
        this.data = data;
    }

    public ActiveChat getActiveChat() {
        return activeChat;
    }

    public void setActiveChat(ActiveChat activeChat) {
        this.activeChat = activeChat;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public static ActiveChatRequestWrapper buildUserChat(ActiveChat activeChat, Object data) {
        return new ActiveChatRequestWrapper(activeChat, data);
    }

    ActiveChatRequestWrapper build(ActiveChat activeChat, Object data) {
        return new ActiveChatRequestWrapper(activeChat, data);
    }
}
