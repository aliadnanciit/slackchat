package com.mywork.slackchat.model;

/**
 * Created by ali_d on 3/23/18.
 */

public class ActiveChat {
    String activeChatId;
    String chatId;
    int count = 0;
    boolean group = false;

    private ActiveChat() {
    }
    private ActiveChat(String chatId, int count, boolean isGroup) {
        this.chatId = chatId;
        this.count = count;
        this.group = isGroup;
    }

    public String getActiveChatId() {
        return activeChatId;
    }

    public void setActiveChatId(String activeChat) {
        this.activeChatId = activeChat;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isGroup() {
        return group;
    }

    public void setGroup(boolean group) {
        group = group;
    }

    public static ActiveChat buildUserChat(String chatId, int count) {
        return new ActiveChat(chatId, count, false);
    }
    public static ActiveChat buildGroupChat(String chatId, int count, boolean isGroup) {
        return new ActiveChat(chatId, count, isGroup);
    }
}
