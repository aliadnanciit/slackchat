package com.mywork.slackchat.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

/**
 * Created by ali_d on 3/20/18.
 */

public class User implements Parcelable {
    String id;
    String nickName;
    HashMap<String, ActiveChat> activeChats;


    private User() {}
    private User(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public HashMap<String, ActiveChat> getActiveChats() {
        return activeChats;
    }
    public void setActiveChats(HashMap<String, ActiveChat> activeChats) {
        this.activeChats = activeChats;
    }

    public static User toUser(String id, String nickName) {
        return toUser(id, nickName, null);
    }
    public static User toUser(String id, String nickName, HashMap<String, ActiveChat> activeChats) {
        User user = new User();
        if(id != null && !id.isEmpty()) {
            user.setId(id);
        }
        if(nickName != null && !nickName.isEmpty()) {
            user.setNickName(nickName);
        }
        if(activeChats != null) {
            user.setActiveChats(activeChats);
        }

        return user;
    }

    public static boolean isEqual(User user1, User user2) {
        return user1.getId().equals(user2.getId());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.nickName);
        dest.writeSerializable(this.activeChats);
    }

    protected User(Parcel in) {
        this.id = in.readString();
        this.nickName = in.readString();
        this.activeChats = (HashMap<String, ActiveChat>) in.readSerializable();
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
