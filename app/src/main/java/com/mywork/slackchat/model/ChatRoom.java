package com.mywork.slackchat.model;

import java.util.HashMap;

/**
 * Created by ali_d on 3/20/18.
 */

public class ChatRoom {
    String id;
    String chatRoomId;
    String name;
    HashMap<String, Boolean> members;

    public ChatRoom() {}
    private ChatRoom(String name, HashMap<String, Boolean> members) {
        id = name;
        this.name = name;
        this.members = members;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashMap<String, Boolean> getMembers() {
        return members;
    }

    public void setMembers(HashMap<String, Boolean> members) {
        this.members = members;
    }

    public static ChatRoom toBuildChatRoom(String userId, String name) {
        HashMap<String, Boolean> hashMap = new HashMap<>();
        hashMap.put(userId, true);

        return new ChatRoom(name, hashMap);
    }
    public static ChatRoom toBuildChatRoom(ChatRoom chatRoom) {
        ChatRoom c = new ChatRoom();
        c.setId(chatRoom.getId());
        c.setName(chatRoom.getName());
        c.setMembers(chatRoom.getMembers());
        return c;
    }
}
