package com.mywork.slackchat.repository;

import com.mywork.slackchat.common.Response;
import com.mywork.slackchat.model.ActiveChatRequestWrapper;
import com.mywork.slackchat.model.Message;
import com.mywork.slackchat.services.ChatService;
import com.mywork.slackchat.services.RequestCallback;

import javax.inject.Inject;

/* This repository get and manage all operations about chat like
* create new chat for group or between individual user
* read all messages from chat between users
*/

public class ChatRepository implements ChatInterface, RequestCallback{
    private ChatService chatService;

    RequestCallback requestCallback;

    @Inject
    public ChatRepository(ChatService chatService) {
        this.chatService = chatService;
    }



    @Override
    public void onResult(Response response) {
        if(requestCallback == null) {
            return;
        }
        requestCallback.onResult(response);
    }

    @Override
    public void createChat(RequestCallback requestCallback, ActiveChatRequestWrapper activeChatRequestWrapper) {
        this.requestCallback = requestCallback;

        chatService.createChat(this, activeChatRequestWrapper);
    }
//    @Override
    public void loadChat(RequestCallback requestCallback, String chatId) {
        this.requestCallback = requestCallback;

        chatService.loadChat(this, chatId);
    }
    public void sendMessage(RequestCallback requestCallback, String chatId, Message message) {
        this.requestCallback = requestCallback;
        chatService.sendMessage(this, chatId, message);
    }

    @Override
    public void readAllMessages() {
    }

    @Override
    public void sendMessage(String from, String message) {
    }
}
