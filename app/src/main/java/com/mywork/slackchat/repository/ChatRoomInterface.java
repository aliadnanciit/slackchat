package com.mywork.slackchat.repository;

import com.mywork.slackchat.model.ChatRoom;
import com.mywork.slackchat.services.RequestCallback;

public interface ChatRoomInterface {
    void createChatRoom(RequestCallback requestCallback, ChatRoom chatRoom);
    void joinChatRoom(RequestCallback requestCallback, String userId, ChatRoom chatRoom);
    void fetchAllChatRooms(RequestCallback requestCallback);

}
