package com.mywork.slackchat.repository;

import com.mywork.slackchat.common.Response;
import com.mywork.slackchat.model.ChatRoom;
import com.mywork.slackchat.services.ChatRoomService;
import com.mywork.slackchat.services.RequestCallback;

import javax.inject.Inject;

/* This repository get and manage all operations about ChatRooms like
* create new chat room with some name
* fetch list of all all ChatRooms
* join chat room etc
*/
public class ChatRoomRepository implements ChatRoomInterface, RequestCallback {

    ChatRoomService chatRoomService;
    RequestCallback requestCallback;


    @Inject
    public ChatRoomRepository(ChatRoomService chatRoomService) {
        this.chatRoomService = chatRoomService;
    }

    @Override
    public void createChatRoom(RequestCallback requestCallback, ChatRoom chatRoom) {
        this.requestCallback = requestCallback;
        chatRoomService.createChatRoom(this, chatRoom);
    }

    @Override
    public void joinChatRoom(RequestCallback requestCallback, String userId, ChatRoom chatRoom) {
        this.requestCallback = requestCallback;

        chatRoomService.joinChatRoom(this, userId, chatRoom);
    }

    @Override
    public void fetchAllChatRooms(RequestCallback requestCallback) {
        this.requestCallback = requestCallback;
        chatRoomService.fetchAllChatRooms(this);
    }

    @Override
    public void onResult(Response response) {
        if(requestCallback == null) {
            return;
        }
        requestCallback.onResult(response);
    }
}
