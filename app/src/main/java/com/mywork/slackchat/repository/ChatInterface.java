package com.mywork.slackchat.repository;

import com.mywork.slackchat.model.ActiveChatRequestWrapper;
import com.mywork.slackchat.services.RequestCallback;

public interface ChatInterface {
    void createChat(RequestCallback requestCallback, ActiveChatRequestWrapper activeChatRequestWrapper);
    void readAllMessages();
    void sendMessage(String from, String message);
}
