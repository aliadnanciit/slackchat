package com.mywork.slackchat.repository;

import com.mywork.slackchat.services.RequestCallback;

public interface UserInterface {
    void signUpUser(String userId, String nickName);
    void signUpUser(RequestCallback requestCallback, String userId, String nickName);
    void fetchAllUsers(RequestCallback requestCallback);

}
