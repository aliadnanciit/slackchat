package com.mywork.slackchat.repository;

import com.mywork.slackchat.common.Response;
import com.mywork.slackchat.model.User;
import com.mywork.slackchat.services.RequestCallback;
import com.mywork.slackchat.services.UserService;

import java.util.List;

import javax.inject.Inject;

/* This repository get and manage all operations about users like
* create new user with unique nick name
* fetch list of all users
*/

public class UserRepository implements UserInterface, RequestCallback {
    private UserService userService;

    private User user;
    private List<User> users;

    RequestCallback requestCallback;

    @Inject
    public UserRepository(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void signUpUser(String userId, String nickName) {
        userService.signUpUser(this, userId, nickName);
    }

    public void loginUser(RequestCallback requestCallback, String userId) {
        this.requestCallback = requestCallback;
        userService.loginUser(this, userId);
    }

    @Override
    public void signUpUser(RequestCallback requestCallback, String userId, String nickName) {
        this.requestCallback = requestCallback;
        userService.signUpUser(this, userId, nickName);
    }

//    @Override
    public void openChat(RequestCallback requestCallback, User loginUser, User friend) {
        this.requestCallback = requestCallback;
        userService.openChat(this, loginUser, friend);
    }

    public User getCreateUserResult() {
        return user;
    }
    public List<User> getAllUsers() {
        return users;
    }

    @Override
    public void fetchAllUsers(RequestCallback requestCallback) {
        this.requestCallback = requestCallback;
        userService.fetchAllUsers(this);
    }

    public void fetchUserActiveChats(RequestCallback requestCallback, String userId) {
        this.requestCallback = requestCallback;
        userService.fetchUserActiveChats(this, userId);
    }

    @Override
    public void onResult(Response response) {
        if(requestCallback == null) {
            return;
        }
        requestCallback.onResult(response);
    }
}